#!/bin/bash

SCHEMA_URL=$(grep 'httpEndpoint:' ./config.yml | awk '{ print $2 }')

# Replace the placeholder in codegen.template.yml with the extracted value
sed "s|__SCHEMA_URL__|$SCHEMA_URL|" ./scripts/codegen.template.yml > ./codegen.yml
