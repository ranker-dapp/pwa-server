ARG NODE_VERSION=21

FROM node:${NODE_VERSION}-bookworm AS base
RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked <<EOF
set -e
rm -f /etc/apt/apt.conf.d/docker-clean
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
apt-get -y --no-install-recommends install dumb-init
rm -rf /var/lib/apt/lists/*
EOF

ARG BUILD_WORKDIR=/app
WORKDIR $BUILD_WORKDIR

COPY --link --chown=node:node . .

# Dependencies
FROM base AS build

RUN --mount=type=cache,target=$BUILD_WORKDIR/.npm <<EOF
set -e
npm set cache $BUILD_WORKDIR/.npm
npm ci --no-progress
EOF

RUN npm run build


# Development
FROM build AS dev

EXPOSE 3000
CMD ["dumb-init", "npm", "run", "dev"]


# Test
FROM build AS test
RUN npm run lint && npm run test


# Release
FROM node:${NODE_VERSION}-alpine AS release
RUN --mount=target=/var/cache/apk,type=cache,sharing=locked <<EOF
set -e
apk update
apk upgrade
apk add --no-cache dumb-init
EOF

ENV NODE_ENV=production

ARG BUILD_WORKDIR=/app
ARG WORKDIR=/app
WORKDIR $WORKDIR

COPY --chown=node:node --from=build $BUILD_WORKDIR/.npm ./.npm
COPY --chown=node:node --from=build $BUILD_WORKDIR/.output ./.output
COPY --chown=node:node --link config.yml package*.json ./
RUN --mount=type=cache,target=$WORKDIR/.npm <<EOF
set -e
npm set cache $WORKDIR/.npm
npm ci --omit=dev --no-progress
EOF

USER node

ENV HOST 0.0.0.0
ENV PORT=80
EXPOSE $PORT
CMD ["dumb-init", "node", ".output/server/index.mjs"]
