import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
};

export type CreateDimensionInput = {
  /** name */
  name: Scalars['String']['input'];
};

export type CreateItemInput = {
  /** comment */
  comment: Scalars['String']['input'];
  /** title */
  title: Scalars['String']['input'];
};

export type CreateMurmurationInput = {
  /** name */
  name: Scalars['String']['input'];
  /** projection */
  projection: ProjectionInput;
  /** users */
  users: Array<UserInput>;
};

export type CreateProjectionInput = {
  /** dimensions */
  dimensions: Array<DimensionInput>;
  /** name */
  name: Scalars['String']['input'];
};

export type Dimension = {
  __typename?: 'Dimension';
  /** items */
  items?: Maybe<Array<Item>>;
  /** name */
  name: Scalars['String']['output'];
  /** users */
  users?: Maybe<Array<User>>;
};

export type DimensionInput = {
  /** items */
  items?: InputMaybe<Array<ItemInput>>;
  /** name */
  name: Scalars['String']['input'];
  /** users */
  users?: InputMaybe<Array<UserInput>>;
};

export type Item = {
  __typename?: 'Item';
  /** author */
  author?: Maybe<User>;
  /** comment */
  comment: Scalars['String']['output'];
  /** comment date */
  commentDate: Scalars['String']['output'];
  /** dimensions */
  dimensions?: Maybe<Array<Dimension>>;
  /** id */
  id: Scalars['Int']['output'];
  /** title */
  title: Scalars['String']['output'];
  /** author */
  users?: Maybe<Array<User>>;
};

export type ItemInput = {
  /** author */
  author?: InputMaybe<UserInput>;
  /** comment */
  comment: Scalars['String']['input'];
  /** comment date */
  commentDate: Scalars['String']['input'];
  /** dimensions */
  dimensions?: InputMaybe<Array<DimensionInput>>;
  /** id */
  id: Scalars['Int']['input'];
  /** title */
  title: Scalars['String']['input'];
  /** author */
  users?: InputMaybe<Array<UserInput>>;
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  accessToken: Scalars['String']['output'];
  user: User;
};

export type LoginUserInput = {
  password: Scalars['String']['input'];
  username: Scalars['String']['input'];
};

export type Murmuration = {
  __typename?: 'Murmuration';
  /** name */
  name: Scalars['String']['output'];
  /** projection */
  projection?: Maybe<Projection>;
  /** users */
  users?: Maybe<Array<User>>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createDimension: Dimension;
  createItem: Item;
  createMurmuration: Murmuration;
  createProjection: Projection;
  login: LoginResponse;
  removeDimension: Dimension;
  removeItem: Item;
  removeMurmuration: Murmuration;
  removeProjection: Projection;
  signup: User;
  updateDimension: Dimension;
  updateItem: Item;
  updateMurmuration: Projection;
};


export type MutationCreateDimensionArgs = {
  createDimensionInput: CreateDimensionInput;
};


export type MutationCreateItemArgs = {
  createItemInput: CreateItemInput;
};


export type MutationCreateMurmurationArgs = {
  createMurmurationInput: CreateMurmurationInput;
};


export type MutationCreateProjectionArgs = {
  createProjectionInput: CreateProjectionInput;
};


export type MutationLoginArgs = {
  loginUserInput: LoginUserInput;
};


export type MutationRemoveDimensionArgs = {
  name: Scalars['String']['input'];
};


export type MutationRemoveItemArgs = {
  id: Scalars['Int']['input'];
};


export type MutationRemoveMurmurationArgs = {
  name: Scalars['String']['input'];
};


export type MutationRemoveProjectionArgs = {
  name: Scalars['String']['input'];
};


export type MutationSignupArgs = {
  loginUserInput: LoginUserInput;
};


export type MutationUpdateDimensionArgs = {
  updateDimensionInput: UpdateDimensionInput;
};


export type MutationUpdateItemArgs = {
  updateItemInput: UpdateItemInput;
};


export type MutationUpdateMurmurationArgs = {
  updateProjectionInput: UpdateProjectionInput;
};

export type Projection = {
  __typename?: 'Projection';
  /** average ranked dimension */
  dimension?: Maybe<Dimension>;
  /** dimensions */
  dimensions: Array<Dimension>;
  /** name */
  name: Scalars['String']['output'];
};

export type ProjectionInput = {
  /** average ranked dimension */
  dimension?: InputMaybe<DimensionInput>;
  /** dimensions */
  dimensions: Array<DimensionInput>;
  /** name */
  name: Scalars['String']['input'];
};

export type Query = {
  __typename?: 'Query';
  dimension: Dimension;
  dimensions: Array<Dimension>;
  item: Item;
  items: Array<Item>;
  murmuration: Murmuration;
  murmurations: Array<Murmuration>;
  projection: Projection;
  projections: Array<Projection>;
  user: User;
  users: Array<User>;
};


export type QueryDimensionArgs = {
  name: Scalars['String']['input'];
};


export type QueryItemArgs = {
  id: Scalars['Int']['input'];
};


export type QueryMurmurationArgs = {
  name: Scalars['String']['input'];
};


export type QueryProjectionArgs = {
  name: Scalars['String']['input'];
};


export type QueryUserArgs = {
  username: Scalars['String']['input'];
};

export type UpdateDimensionInput = {
  /** current name */
  currentName: Scalars['String']['input'];
  /** name */
  name?: InputMaybe<Scalars['String']['input']>;
  /** new name */
  newName: Scalars['String']['input'];
};

export type UpdateItemInput = {
  /** comment */
  comment: Scalars['String']['input'];
  id: Scalars['Int']['input'];
  /** title */
  title: Scalars['String']['input'];
};

export type UpdateProjectionInput = {
  /** current name */
  currentName: Scalars['String']['input'];
  /** dimensions */
  dimensions: Array<DimensionInput>;
  /** name */
  name: Scalars['String']['input'];
};

export type User = {
  __typename?: 'User';
  /** dimensions */
  dimensions?: Maybe<Array<Dimension>>;
  /** items */
  items?: Maybe<Array<Item>>;
  /** username */
  username: Scalars['String']['output'];
};

export type UserInput = {
  /** dimensions */
  dimensions?: InputMaybe<Array<DimensionInput>>;
  /** items */
  items?: InputMaybe<Array<ItemInput>>;
  /** username */
  username: Scalars['String']['input'];
};

export type AuthUserFragment = { __typename?: 'User', username: string };

export type LoginMutationVariables = Exact<{
  input: LoginUserInput;
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'LoginResponse', accessToken: string, user: { __typename?: 'User', username: string } } };

export type SignupMutationVariables = Exact<{
  input: LoginUserInput;
}>;


export type SignupMutation = { __typename?: 'Mutation', signup: { __typename?: 'User', username: string } };

export const AuthUserFragmentDoc = {"kind":"Document","definitions":[{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AuthUser"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"username"}}]}}]} as unknown as DocumentNode<AuthUserFragment, unknown>;
export const LoginDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"login"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"input"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"LoginUserInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"login"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"loginUserInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"input"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"user"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AuthUser"}}]}},{"kind":"Field","name":{"kind":"Name","value":"accessToken"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AuthUser"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"username"}}]}}]} as unknown as DocumentNode<LoginMutation, LoginMutationVariables>;
export const SignupDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"mutation","name":{"kind":"Name","value":"signup"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"input"}},"type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"LoginUserInput"}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"signup"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"loginUserInput"},"value":{"kind":"Variable","name":{"kind":"Name","value":"input"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"FragmentSpread","name":{"kind":"Name","value":"AuthUser"}}]}}]}},{"kind":"FragmentDefinition","name":{"kind":"Name","value":"AuthUser"},"typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"User"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"username"}}]}}]} as unknown as DocumentNode<SignupMutation, SignupMutationVariables>;