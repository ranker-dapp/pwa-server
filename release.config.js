module.exports = {
  branches: [
    '+([0-9])?(.{+([0-9]),x}).x',
    'main',
    'next',
    'next-major', {
      name: 'beta',
      prerelease: true
    }, {
      name: 'alpha',
      prerelease: true
    }
  ],
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/gitlab',
    ['@semantic-release/npm', {
      npmPublish: false
    }],
    ['@semantic-release/changelog',
      {
        changelogFile: 'docs/CHANGELOG.md',
        changelogTitle: '# Ranker Progressive Web App Changelog'
      }],
    ['@semantic-release/git',
      {
        assets: [
          'package.json',
          'docs/CHANGELOG.md'
        ],
        // eslint-disable-next-line no-template-curly-in-string
        message: 'chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}'
      }],
      ['@eclass/semantic-release-docker', {
        baseImageName: 'pwa-server:dev',
        releaseImageTag: 'latest-dev',
        registries: [{
          url: 'registry.gitlab.com',
          imageName: 'registry.gitlab.com/ranker-dapp/pwa-server',
          user: 'CI_REGISTRY_USER',
          password: 'CI_REGISTRY_PASSWORD'
        }, {
          url: 'docker.io',
          imageName: 'docker.io/rankerdapp/ranker-pwa',
          user: 'DOCKER_REGISTRY_USER',
          password: 'DOCKER_REGISTRY_PASSWORD'
        }],
        additionalTags: ['next-dev', 'next-major-dev', 'beta-dev', 'alpha-dev']
      }],
      ['@eclass/semantic-release-docker', {
        baseImageName: 'pwa-server',
        releaseImageTag: 'latest',
        registries: [{
          url: 'registry.gitlab.com',
          imageName: 'registry.gitlab.com/ranker-dapp/pwa-server',
          user: 'CI_REGISTRY_USER',
          password: 'CI_REGISTRY_PASSWORD'
        }, {
          url: 'docker.io',
          imageName: 'docker.io/rankerdapp/ranker-pwa',
          user: 'DOCKER_REGISTRY_USER',
          password: 'DOCKER_REGISTRY_PASSWORD'
        }],
        additionalTags: ['next', 'next-major', 'beta', 'alpha']
      }]
  ]
}
