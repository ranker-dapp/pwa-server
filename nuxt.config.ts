import { defineNuxtConfig } from 'nuxt/config';
import { Configurer } from '@deposition.cloud/conf';
import { md3 } from 'vuetify/blueprints';

const conf = new Configurer({
  packageLockJsonFilePath: 'package-lock.json',
  files: process.env.NODE_ENV === 'production' ? ['config.yml']: ['config.yml', 'config.dev.yml'],
  defaults: {
    'devServer': 3000
  },
  required: ['graphql:httpEndpoint', 'graphql:wsEndpoint']
});

const { httpEndpoint, wsEndpoint } = conf.get('graphql');
const { port } = conf.get('devServer');

export default defineNuxtConfig({
  plugins: [],
  modules: [
    '@nuxt/devtools',
    '@nuxtjs/apollo',
    '@pinia/nuxt',
    '@invictus.codes/nuxt-vuetify',
  ],
  apollo: {
    autoImports: true,
    clients: {
      default: {
        httpEndpoint,
        wsEndpoint: undefined,
      },
    },
  },
  devtools: {
    enabled: false,
    vscode: {},
  },
  vite: {
    server: {
      fs: {
        strict: false,
      },
    },
  },
  vuetify: {
    vuetifyOptions: {
      blueprint: md3,
    },
    moduleOptions: {
      treeshaking: true,
      useIconCDN: true,
      styles: true,
      autoImport: true,
      useVuetifyLabs: true,
    },
  },
  imports: {
    dirs: ['./stores', './model', './api'],
  },
  build: {
    transpile: [
      'tslib',
      '@apollo/client/core',
      '@vue/apollo-composable',
      'vuetify',
    ],
  },
  runtimeConfig: {
    baseUrl: httpEndpoint,
    public: {
      baseUrl: httpEndpoint,
    },
  },
  devServer: {
    port
  }
});
