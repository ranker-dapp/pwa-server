import { Configurer } from '@deposition.cloud/conf';

const conf = new Configurer({
  packageLockJsonFilePath: 'package-lock.json',
  required: ['graphql:httpEndpoint', 'graphql:wsEndpoint'],
  files: ['config.yml'],
});

const { httpEndpoint, wsEndpoint } = conf.get('graphql');

module.exports = {
  client: {
    service: {
      url: httpEndpoint
    },
    includes: ['api/queries/*.gql']
  }
}