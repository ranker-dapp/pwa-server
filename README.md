# PWA

## Running the Application Locally

This application is Dockerized and can be run either directly or inside Docker containers.

### Directly

1. Install Node.js and NPM on your system.
2. Clone this repository.
3. Install dependencies with `npm install`.
4. Run the application in development mode with `npm run dev`.

### With Docker

This application uses a multi-stage Dockerfile that can set up different environments in Docker containers. Here are the steps to build and run the application in Docker:

#### Development

1. Build the development image: `docker build --target dev -t ranker-pwa:dev .`.
2. Run the container: `docker run -p 3000:3000 -v $(pwd):/app ranker-pwa:dev`. This will start the application in development mode and map the local directory to the `/app` directory in the container, allowing changes in your local files to be reflected in the running container.

#### Production

1. Build the production image: `docker build --target release -t ranker-pwa:release .`.
2. Run the container: `docker run -p 3000:3000 ranker-pwa:release`. This will start the application in production mode.

**Note:** Nuxt.js doesn't support hot module reloading out-of-the-box when the server is running inside a Docker container because it uses websockets to notify of file changes. You will need to make some extra adjustments for hot reloading to work in a Docker container.

### Testing

The Dockerfile includes a separate `test` target that can be used to run your tests in a Docker container:

1. Build the test image: `docker build --target test -t ranker-pwa:test .`.
2. Run the container: `docker run ranker-pwa:test`. This will run your tests and then stop the container.

## Contributing

Contributions are welcome! Please fork the repository and create a pull request with your changes.

## License

This project is ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/). It's like open source, for good.
