import { defineStore } from 'pinia';
import { ref } from 'vue';
import gql from 'graphql-tag';
import type { Murmuration } from '~/api/generated/types';

const MURMURATIONS_QUERY = gql`
    query murmurations {
      murmurations {
        name
        users {
          username
        }
        projection {
          name
          dimension {
            name
            items {
              id
              title
              comment
              commentDate
            }
          }
        }
      }
    }
  `;

const useMurmurationsStore = defineStore('murmurationStore', () => {
  const murmurations: Ref<Murmuration[]> = ref([]);

  const loadData = async () => {
    const { data } = await useAsyncQuery(MURMURATIONS_QUERY);
    murmurations.value = (data.value as { murmurations?: Murmuration[] })?.murmurations ?? [];
  };

  return { murmurations, loadData }
});

export {
  useMurmurationsStore,
  MURMURATIONS_QUERY,
}
