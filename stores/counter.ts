function useCounter () {
  const count = ref(0);
  const double = computed(() => count.value * 2);

  const increment = () => {
    count.value++;
  };

  return { count, double, increment };
}

export const useCounterStore = defineStore('counter', useCounter);