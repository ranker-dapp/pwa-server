import { defineStore } from 'pinia';
import { ref } from 'vue';
import gql from 'graphql-tag';
import type { Dimension, User } from '~/api/generated/types';

const DIMENSIONS_QUERY = gql`
    query	users {
      users {
        username
        dimensions {
          name
          items {
            title
            comment
            commentDate
          }
        }
      }
    }
  `;

const useDimensionsStore = defineStore('murmurationStore', () => {
  const dimensions: Ref<Dimension[]> = ref([]);

  const loadData = async () => {
    const { data } = await useAsyncQuery(DIMENSIONS_QUERY);

    dimensions.value = (data.value as {
      dimensions?: Dimension[]
    })?.dimensions ?? [];
  };

  return { dimensions, loadData }
});

export {
  useDimensionsStore,
  DIMENSIONS_QUERY,
}
