import { defineStore } from 'pinia';
import { ref, onMounted } from 'vue';
import gql from 'graphql-tag';
import { provideApolloClient, useSubscription } from '@vue/apollo-composable'
import type { Item } from '~/api/generated/types';

const ITEMS_QUERY = gql`
    query GetItems {
      items {
        id
        title
        comment
        commentDate
      }
    }
  `;

const ITEMS_SUBSCRIPTION = gql`
    subscription GetUpdates {
      itemsUpdated {
        id,
        title,
        comment,
        commentDate
      }
    }
  `;

const useItemsStore = defineStore('itemsStore', () => {
  const items = ref<Item[]>([]);

  const loadItems = async () => {
    const { data } = await useAsyncQuery(ITEMS_QUERY);
    const retrievedItems = (data.value as { items?: Item[] })?.items
    if (retrievedItems) {
      items.value = [...retrievedItems];
    }
  };

  return { items, loadItems }
});

export {
  useItemsStore,
  ITEMS_QUERY,
  ITEMS_SUBSCRIPTION
}
