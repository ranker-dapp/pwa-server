import { defineStore } from 'pinia';
import { ref } from 'vue';
import gql from 'graphql-tag';
import type { LoginResponse } from '~/api/generated/types';
import type { OperationVariables } from '@apollo/client/core';

const LOGIN_MUTATION = gql`
  mutation login($input: LoginUserInput!) {
    login (loginUserInput: $input) {
      user {
        username
      }
      accessToken
    }
  }`;

const useAuthStore = defineStore('authStore', () => {
  const auth: Ref<LoginResponse | undefined> = ref();

  const loadData = async (username: string, password: string) => {
    const variables: OperationVariables = {
      input: {
        username,
        password
      }
    }
    const { mutate } = useMutation(LOGIN_MUTATION, { variables })
    const result = await mutate()
    auth.value = result?.data.login;
  };

  return { auth, loadData }
});

export {
  useAuthStore,
  LOGIN_MUTATION,
}
