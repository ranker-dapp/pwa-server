# Ranker Progressive Web App Changelog

## [1.0.3](https://gitlab.com/ranker-dapp/pwa-server/compare/v1.0.2...v1.0.3) (11/15/2023)


### Bug Fixes

* **types:** import types and remove unused functionality ([62703c1](https://gitlab.com/ranker-dapp/pwa-server/commit/62703c16fcd7e4cb469fcf7926cd23ccb1f6c721))

# Ranker Server App Changelog

## [1.0.2](https://gitlab.com/ranker-dapp/pwa-server/compare/v1.0.1...v1.0.2) (11/15/2023)


### Bug Fixes

* **release:** also publish latest-dev ([2e019e6](https://gitlab.com/ranker-dapp/pwa-server/commit/2e019e65eeacc4f751a8c8c4190ff176ed17c274))

## [1.0.1](https://gitlab.com/ranker-dapp/pwa-server/compare/v1.0.0...v1.0.1) (11/15/2023)


### Bug Fixes

* **deps:** update to latest ([05b9f21](https://gitlab.com/ranker-dapp/pwa-server/commit/05b9f214a0e0f1e1f344a1b47494867d64c6b57d))

## [1.0.1](https://gitlab.com/ranker-dapp/pwa-server/compare/v1.0.0...v1.0.1) (11/15/2023)


### Bug Fixes

* **deps:** update to latest ([05b9f21](https://gitlab.com/ranker-dapp/pwa-server/commit/05b9f214a0e0f1e1f344a1b47494867d64c6b57d))

## [1.0.1](https://gitlab.com/ranker-dapp/pwa-server/compare/v1.0.0...v1.0.1) (11/15/2023)


### Bug Fixes

* **deps:** update to latest ([05b9f21](https://gitlab.com/ranker-dapp/pwa-server/commit/05b9f214a0e0f1e1f344a1b47494867d64c6b57d))

# 1.0.0 (2023-11-07)


### Bug Fixes

* **deps:** update dependencies ([5e0fbc2](https://gitlab.com/ranker-dapp/pwa-server/commit/5e0fbc221dba6f6888f2cfa8cf5d3495d813d067))


### Features

* **config:** add runtime configs ([d6d0cb7](https://gitlab.com/ranker-dapp/pwa-server/commit/d6d0cb739f53a643b51ca917c3e711e7b89a465b))
* **data:** load data via graphql query and subscriptions ([a83e8f3](https://gitlab.com/ranker-dapp/pwa-server/commit/a83e8f3e401f98b732632766cf740a1ba8cc7dc6))
* **dimension:** add candidates to murmuration dimension view ([1a472df](https://gitlab.com/ranker-dapp/pwa-server/commit/1a472dfa4bc3d33289eca08a820ac67ba5c6579d))
* **dimensions:** add dimensions view ([967b8c7](https://gitlab.com/ranker-dapp/pwa-server/commit/967b8c722251e3e854649415d2c353d08bd59b8a))
* **docker:** add docker compose definition ([db1a44b](https://gitlab.com/ranker-dapp/pwa-server/commit/db1a44b1bb9ba14d35be862b81b297a882983ac4))
* **items:** retrieve and show items via graphql ([1c01617](https://gitlab.com/ranker-dapp/pwa-server/commit/1c0161758c6848ab720a9a2ab6ac9d70bbf40354))
* **login:** add signin flow and vuetify ([23fb17f](https://gitlab.com/ranker-dapp/pwa-server/commit/23fb17f596f12a353968591b25098a969d54c996))
* **murmurations:** show murmurations in data table ([bf1e16d](https://gitlab.com/ranker-dapp/pwa-server/commit/bf1e16d1803b12fd31da96827370e1ea9ce5dba6))
* **router:** add pagination ([b2c6806](https://gitlab.com/ranker-dapp/pwa-server/commit/b2c68060863bc629a8582dd0a8b19ae52864e03c))
* **server:** parametrize configurations ([426fcdc](https://gitlab.com/ranker-dapp/pwa-server/commit/426fcdcf9b3670cefe9d8b489e6d71b276a4b331))
* **stores:** use composition api for pinia stores ([9a51bb1](https://gitlab.com/ranker-dapp/pwa-server/commit/9a51bb10ffa5ace899a8778b2856c8f3693122bb))
* **subscriptions:** use newer ws server ([e87aacd](https://gitlab.com/ranker-dapp/pwa-server/commit/e87aacda88d20fb0ef6e4660304a156407a0c2c2))
